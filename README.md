# README #

This is a simple idle game prototype. 

There is an enemy at the center of the screen. Player can earn gold by tapping that enemy.
The gold income can be increased by upgrade system.

There are 5 unlockable idle attackers around the enemy. Idle attackers provide passive income.

* The game is tested on actual android and ios devices. (iPad Mini 4 - ios 13.3, xiaomi redmi 5 plus - android 7.1.2)
*  [Playable apk download link](https://drive.google.com/file/d/1ZUqKIVHPUMPw9fi5pSq5Im_3RW84ka2z/view?usp=sharing)

### How does remote config work? ###
* The game makes a request from [this public repo](https://gist.github.com/aykutyilmaz1/1ee63deaddd015b78e42d718c6daaceb)
 to get the required game configs on start.
 * In case of network error, local data will be used.
 * Changes can take couple of minutes to apply client side because of the caching system in github.
 * If there is a format error with the config file, client side tries to start the game with the default config which is stored in the main memory. 
 * If there is a missing variable or naming mistake in the config file client side also tries to use it from
 default configs.
 * Although there are 2 level of protection be careful while editing configs.
 * The config file includes variables for the formulas which is used for calculating upgrade costs, 
 unlock costs, and gold incomes.

### Default game config and formulas ###

```
{
  "goldConfig": {
    "multiplier": 5.0,
    "power": 2.1
  },
  "upgradeConfig": {
    "multiplier": 5.0,
    "powerBase": 1.08
  },
  "unlockAttackerConfig": {
    "baseCost": 100,
    "multiplier": 10
  }
}
```

```
gold income per hit = goldConfig.multiplier * (level ^ goldConfig.power)

upgrade cost for level = upgradeConfig.multiplier * (upgradeConfig.powerBase ^ level)

unlock attacker cost = unlockAttackerConfig.baseCost * 
    (unlockAttackerConfig.multiplier ^ unlocked attacker count)
```

### Who do I talk to? ###

* [Contact for further information](mailto:aykutylmz125@gmail.com)
