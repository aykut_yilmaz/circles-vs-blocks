﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Assertions;

public class UiController : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI goldText;
    [SerializeField] private GameObject upgradeButtonPrefab;
    [SerializeField] private GameObject unlockButtonPrefab;

    private Camera _camera;
    
    private void OnValidate()
    {
        Assert.IsNotNull(goldText);
        Assert.IsNotNull(upgradeButtonPrefab);
        Assert.IsNotNull(unlockButtonPrefab);
    }

    private void Start()
    {
        _camera = Camera.main;
    }

    public void SetGoldText(int gold)
    {
        goldText.SetText(gold.ToString());
    }

    public UnlockButton CreateUnlockButton(Vector3 worldPos)
    {
        var go = Instantiate(unlockButtonPrefab, transform);
        go.transform.position = _camera.WorldToScreenPoint(worldPos);
        return go.GetComponent<UnlockButton>();
    }
    
    public UpgradeButton CreateUpgradeButton(Vector3 worldPos)
    {
        var go = Instantiate(upgradeButtonPrefab, transform);
        go.transform.position = _camera.WorldToScreenPoint(worldPos);
        return go.GetComponent<UpgradeButton>();
    }
}
