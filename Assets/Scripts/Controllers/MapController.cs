﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class MapController : MonoBehaviour
{
    [SerializeField] private Actor mainActor;
    [SerializeField] private UpgradeButton mainUpgradeButton;
    [SerializeField] private List<Vector2> idleActorPositions;
    [SerializeField] private GameObject idleActorPrefab;
    
    private UiController _uiController;
    private List<Actor> _actors;
    
    private void OnValidate()
    {
        Assert.IsNotNull(mainActor);
        Assert.IsNotNull(mainUpgradeButton);
        Assert.IsNotNull(idleActorPrefab);
    }

    public void SetUiController(UiController uiController)
    {
        _uiController = uiController;
    }
    
    public List<Actor> GetActors()
    {
        return _actors;
    }

    public void CreateMap()
    {
        _actors = new List<Actor>();
        InitializeMainActor();
        CreateIdleActors();
    }
    
    private void CreateIdleActors()
    {
        for (int i = 0; i < idleActorPositions.Count; i++)
        {
            var go = Instantiate(idleActorPrefab, transform);
            go.transform.position = idleActorPositions[i];
            var actor = go.GetComponent<Actor>();

            var buttonPosition = 
                go.transform.position + Vector3.down * .85f;
            actor.SetUpgradeButton(
                _uiController.CreateUpgradeButton(buttonPosition));
            ((IdleAttacker)actor).SetUnlockButton(
                _uiController.CreateUnlockButton(buttonPosition));
            
            _actors.Add(actor);
        }
    }
    
    private void InitializeMainActor()
    {
        mainActor.SetUpgradeButton(mainUpgradeButton);
        _actors.Add(mainActor);
    }
}
