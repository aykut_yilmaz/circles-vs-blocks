﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FxController : MonoBehaviour
{
    private CoinAnimationPool _coinAnimationPool;
    private AttackAnimationPool _attackAnimationPool;
    
    private void Start()
    {
        _coinAnimationPool = GetComponent<CoinAnimationPool>();
        _attackAnimationPool = GetComponent<AttackAnimationPool>();
    }

    public void AnimateCoin(int count)
    {
        var coin = _coinAnimationPool.GetCoinAnimation();
        coin.Animate(count);
    }

    public void AnimateAttack(Vector3 source, Vector3 destination)
    {
        var animation = _attackAnimationPool.GetAttackAnimation();
        animation.Animate(source, destination);
    }
}
