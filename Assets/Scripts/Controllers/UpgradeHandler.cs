﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class UpgradeHandler : MonoBehaviour
{
    private GameController _gameController;
    private GoldPerTapConfig _goldPerTapConfig;
    private UpgradeCostConfig _upgradeCostConfig;
    private UnlockAttackerConfig _unlockAttackerConfig;
    
    private List<Actor> _actors;
    private int _unlockedAttackerCount;
    private int _gold;
    
    public void SetGameConfig(GameConfig gameConfig)
    {
        _goldPerTapConfig = gameConfig.goldConfig;
        _upgradeCostConfig = gameConfig.upgradeConfig;
        _unlockAttackerConfig = gameConfig.unlockAttackerConfig;
    }

    public void SetGameController(GameController gameController)
    {
        _gameController = gameController;
        _gameController.onGoldChange += OnGoldChange;
    }

    private void OnGoldChange(int gold)
    {
        _gold = gold;
        
        foreach (var actor in _actors)
        {
            actor.UpdateButtons(gold);
        }
    }

    public void InitializeActors(List<Actor> actors)
    {
        _actors = actors;

        foreach (var actor in _actors)
        {
            actor.Level = 1;
            UpdateActor(actor);
            actor.onUpgradeRequest += Actor_OnUpgradeRequest;

            if (actor is IdleAttacker attacker)
            {
                attacker.onUnlockRequest += Attacker_OnUnlockRequest;
                UpdateIdleAttacker(attacker);
            }
        }
    }
    
    private void Actor_OnUpgradeRequest(Actor actor)
    {
        var requiredGold = actor.UpgradeCost;
        
        if (_gameController.HasEnoughGold(requiredGold))
        {
            actor.Level++;
            actor.AnimateUpgrade();
            _gameController.PurchaseGold(requiredGold);
            UpdateActor(actor);
        }
    }

    private void UpdateActor(Actor actor)
    {
        actor.Income = GetGoldIncome(actor.Level);
        actor.UpgradeCost = GetUpgradeCost(actor.Level);
        actor.UpdateButtons(_gold);
    }

    private void UpdateIdleAttacker(IdleAttacker attacker)
    {
        if (!attacker.IsUnlocked)
        {
            attacker.UnlockCost = GetUnlockCost();
            attacker.UpdateButtons(_gold);
        }
    }
    
    private void Attacker_OnUnlockRequest(IdleAttacker attacker)
    {
        var requiredGold = attacker.UnlockCost;
        
        if (_gameController.HasEnoughGold(requiredGold))
        {
            _unlockedAttackerCount++;
            attacker.Unlock();
            _gameController.PurchaseGold(requiredGold);
            _gameController.RegisterNewIdleAttacker(attacker);
            UpdateAllAttackers();
        }
    }

    private void UpdateAllAttackers()
    {
        foreach (var actor in _actors)
        {
            if (actor is IdleAttacker attacker)
            {
                UpdateIdleAttacker(attacker);
            }
        }
    }

    private int GetGoldIncome(int level)
    {
        var value = _goldPerTapConfig.multiplier *
                    Mathf.Pow(level, _goldPerTapConfig.power);
        
        return (int)value;
    }
    
    private int GetUpgradeCost(int level)
    {
        var value = _upgradeCostConfig.multiplier *
                    Mathf.Pow(_upgradeCostConfig.powerBase, level);
        
        return (int)value;
    }

    private int GetUnlockCost()
    {
        return _unlockAttackerConfig.baseCost *
               (int)Mathf.Pow(_unlockAttackerConfig.multiplier,
                         _unlockedAttackerCount);
    }
}
