﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public class GameController : MonoBehaviour
{
    public UnityAction<int> onGoldChange;
    
    [SerializeField] private Enemy enemy;
    [SerializeField] private FxController fxController;
    [SerializeField] private UiController uiController;
    [SerializeField] private UpgradeHandler upgradeHandler;
    [SerializeField] private MapController mapController;

    private int _gold;
    private List<IdleAttacker> _activeIdleAttackers;
    private ConfigHandler _configHandler;
    private void OnValidate()
    {
        Assert.IsNotNull(enemy);
        Assert.IsNotNull(fxController);
        Assert.IsNotNull(uiController);
        Assert.IsNotNull(upgradeHandler);
        Assert.IsNotNull(mapController);
    }

    void Start()
    {        
        _activeIdleAttackers = new List<IdleAttacker>();
        _configHandler = GetComponent<ConfigHandler>();
        InitializeConfigs();
    }

    public bool HasEnoughGold(int amount)
    {
        return _gold >= amount;
    }

    public void PurchaseGold(int amount)
    {
        _gold -= amount;
        onGoldChange?.Invoke(_gold);
        uiController.SetGoldText(_gold);
    }

    public void RegisterNewIdleAttacker(IdleAttacker idleAttacker)
    {
        _activeIdleAttackers.Add(idleAttacker);
        idleAttacker.onAttack += Attacker_OnAttack;
    }
    
    private void InitializeConfigs()
    {
        _configHandler.StartServerRequest();
        _configHandler.onConfigDownload += OnConfigDownload;
    }

    private void OnConfigDownload(GameConfig config)
    {
        upgradeHandler.SetGameConfig(config);
        InitializeGameComponents();
    }
    
    private void InitializeGameComponents()
    {
        mapController.SetUiController(uiController);
        mapController.CreateMap();
        upgradeHandler.SetGameController(this);
        upgradeHandler.InitializeActors(mapController.GetActors());
        enemy.onClick += Enemy_OnClick;
    }

    private void Enemy_OnClick()
    {
        HandleGoldIncome(enemy.Income);
    }
    
    private void Attacker_OnAttack(IdleAttacker attacker)
    {
        fxController.AnimateAttack(attacker.GetPosition(), 
            enemy.GetPosition());
        HandleGoldIncome(attacker.Income);
    }

    private void HandleGoldIncome(int income)
    {
        _gold += income;
        onGoldChange?.Invoke(_gold);
        uiController.SetGoldText(_gold);
        fxController.AnimateCoin(income);
    }

    private void Update()
    {
        foreach (var attacker in _activeIdleAttackers)
        {
            attacker.Step(Time.deltaTime);
        }
    }
}
