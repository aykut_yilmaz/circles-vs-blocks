﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public class Actor : MonoBehaviour
{
    [SerializeField] private ParticleSystem upgradeParticle;
    public UnityAction<Actor> onUpgradeRequest;
    
    public int Level { get; set; }
    public int Income { get; set; }
    public int UpgradeCost { get; set; }

    protected UpgradeButton _upgradeButton;
    protected SpriteRenderer _spriteRenderer;

    void Start()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }
    
    public void SetUpgradeButton(UpgradeButton upgradeButton)
    {
        _upgradeButton = upgradeButton;
        upgradeButton.onClick += OnClickUpgrade;
    }

    public Vector3 GetPosition()
    {
        return transform.position;
    }
    
    public virtual void UpdateButtons(int gold)
    {
        _upgradeButton.SetLevel(Level);
        _upgradeButton.SetCost(UpgradeCost);
        _upgradeButton.SetAffordable(gold >= UpgradeCost);
    }

    public void AnimateUpgrade()
    {
        upgradeParticle.Play();
    }

    private void OnClickUpgrade()
    {
        onUpgradeRequest?.Invoke(this);
    }
}
