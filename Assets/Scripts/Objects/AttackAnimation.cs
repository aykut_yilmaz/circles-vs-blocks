﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class AttackAnimation : MonoBehaviour
{
    private void Start()
    {
        gameObject.SetActive(false);
    }
    
    public void Animate(Vector3 source, Vector3 destination)
    {
        gameObject.SetActive(true);
        transform.position = source;
        transform.DOMove(destination, .25f).OnComplete(delegate
        {
            gameObject.SetActive(false);
        });
    }
}
