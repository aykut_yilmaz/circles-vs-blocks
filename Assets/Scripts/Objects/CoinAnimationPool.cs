﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class CoinAnimationPool : MonoBehaviour
{
    [SerializeField] private GameObject coinPrefab;
    
    private List<CoinAnimation> _coinAnimations;
    private int _coinIndex;

    private void OnValidate()
    {
        Assert.IsNotNull(coinPrefab);
    }
    
    private void Start()
    {
        InitializeCoins();
    }
    
    public CoinAnimation GetCoinAnimation()
    {
        _coinIndex++;
        _coinIndex %= _coinAnimations.Count;
        return _coinAnimations[_coinIndex];
    }

    private void InitializeCoins()
    {
        _coinAnimations = new List<CoinAnimation>();
        
        for (int i = 0; i < 5; i++)
        {
            var go = Instantiate(coinPrefab, transform);
            _coinAnimations.Add(go.GetComponent<CoinAnimation>());
        }
    }
}
