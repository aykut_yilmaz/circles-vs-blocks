﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;

public class Enemy : Actor
{
    public UnityAction onClick;

    private Sequence _jumpSequence;
    
    private void OnMouseDown()
    {
        AnimateClick();
        onClick?.Invoke();
    }

    private void AnimateClick()
    {
        _jumpSequence?.Kill();
        _jumpSequence = DOTween.Sequence();
        _jumpSequence.Append(transform.DOMoveY(.15f, .1f));
        _jumpSequence.Append(transform.DOMoveY(0, .1f));
    }
}
