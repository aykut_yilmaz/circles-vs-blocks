﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;
using UnityEngine.Assertions;
using Random = UnityEngine.Random;

public class CoinAnimation : MonoBehaviour
{
    [SerializeField] private TextMeshPro countText;
    private Sequence _animationSequence;

    private void OnValidate()
    {
        Assert.IsNotNull(countText);
    }

    private void Start()
    {
        gameObject.SetActive(false);
    }

    public void Animate(int count)
    {
        _animationSequence?.Kill();
        gameObject.SetActive(true);
        countText.SetText(count.ToString());
        
        transform.localScale = Vector3.zero;
        transform.position = new Vector2(Random.Range(-.3f, .3f), 0);

        _animationSequence = DOTween.Sequence();
        
        _animationSequence.Append(transform.DOScale(1, .25f));
        _animationSequence.Join(transform.DOMoveY(1.2f, .25f));
        _animationSequence.Append(transform.DOScale(0, .25f));

        _animationSequence.AppendCallback(delegate
        {
            gameObject.SetActive(false);
        });
    }
}
