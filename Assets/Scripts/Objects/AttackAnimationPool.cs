﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class AttackAnimationPool : MonoBehaviour
{
    [SerializeField] private GameObject animationPrefab;

    private List<AttackAnimation> _attackAnimations;
    private int _animationIndex;

    private void OnValidate()
    {
        Assert.IsNotNull(animationPrefab);
    }

    private void Start()
    {
        InitializeAnimations();
    }

    public AttackAnimation GetAttackAnimation()
    {
        _animationIndex++;
        _animationIndex %= _attackAnimations.Count;
        return _attackAnimations[_animationIndex];
    }
    
    private void InitializeAnimations()
    {
        _attackAnimations = new List<AttackAnimation>();
        
        for (int i = 0; i < 10; i++)
        {
            var go = Instantiate(animationPrefab, transform);
            _attackAnimations.Add(go.GetComponent<AttackAnimation>());
        }
    }
}
