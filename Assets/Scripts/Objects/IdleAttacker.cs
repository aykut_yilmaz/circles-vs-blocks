﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class IdleAttacker : Actor
{
    [SerializeField] private ParticleSystem unlockParticle;
    
    public UnityAction<IdleAttacker> onUnlockRequest;
    public UnityAction<IdleAttacker> onAttack;
    public int UnlockCost { get; set; }

    private UnlockButton _unlockButton;
    private float _updateTimer;
    private bool _isUnlocked;
    public bool IsUnlocked => _isUnlocked;
    
    public void SetUnlockButton(UnlockButton unlockButton)
    {
        _unlockButton = unlockButton;
        _unlockButton.onClick += OnClickUnlock;
        _upgradeButton.SetActive(false);
    }
    
    public void Unlock()
    {
        _isUnlocked = true;
        _upgradeButton.SetActive(true);
        _unlockButton.SetActive(false);
        AnimateUnlock();
    }

    public override void UpdateButtons(int gold)
    {
        base.UpdateButtons(gold);
        _unlockButton.SetCost(UnlockCost);
        _unlockButton.SetAffordable(gold >= UnlockCost);
    }
    
    public void Step(float deltaTime)
    {
        _updateTimer += deltaTime;

        if (_updateTimer >= 1.0f)
        {
            _updateTimer = 0;
            onAttack?.Invoke(this);
        }
    }

    private void AnimateUnlock()
    {
        _spriteRenderer.color = Color.white;
        unlockParticle.Play();
    }
    
    private void OnClickUnlock()
    {
        onUnlockRequest?.Invoke(this);
    }
}
