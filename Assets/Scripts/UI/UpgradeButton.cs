﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Assertions;
using UnityEngine.Events;
using UnityEngine.UI;

public class UpgradeButton : GameButton
{
    public void SetLevel(int level)
    {
        titleText.SetText("Level {0}", level);
    }
}
