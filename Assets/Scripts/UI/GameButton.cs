﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;
using UnityEngine.UI;

public class GameButton : MonoBehaviour
{
    public UnityAction onClick;
    
    [SerializeField] protected TextMeshProUGUI titleText;
    [SerializeField] private TextMeshProUGUI coinText;
    [SerializeField] private Sprite notAffordableSprite;
    [SerializeField] private Sprite affordableSprite;

    private Button _button;
    private bool _isAffordable = true;
    
    private void OnValidate()
    {
        Assert.IsNotNull(titleText);
        Assert.IsNotNull(coinText);
        Assert.IsNotNull(notAffordableSprite);
        Assert.IsNotNull(affordableSprite);
    }

    private void Awake()
    {
        _button = GetComponent<Button>();
        _button.onClick.AddListener(OnClick);
    }

    public void SetCost(int gold)
    {
        coinText.SetText(gold.ToString());
    }
    
    public void SetActive(bool active)
    {
        gameObject.SetActive(active);
    }

    public void SetAffordable(bool affordable)
    {
        if (_isAffordable != affordable)
        {
            _button.enabled = affordable;
            
            if (affordable)
            {
                _button.image.sprite = affordableSprite;
            }
            else
            {
                _button.image.sprite = notAffordableSprite;
            }

            _isAffordable = affordable;
        }
    }

    private void OnClick()
    {
        onClick?.Invoke();
    }
}
