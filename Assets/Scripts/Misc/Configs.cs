﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameConfig
{
    public GoldPerTapConfig goldConfig;
    public UpgradeCostConfig upgradeConfig;
    public UnlockAttackerConfig unlockAttackerConfig;
    
    public static GameConfig CreateFromJSON(string jsonString)
    {
        try
        {
            return JsonUtility.FromJson<GameConfig>(jsonString);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            return null;
        }
    }

    public void CreateSample()
    {
        Debug.Log(JsonUtility.ToJson(this));
    }
}

[System.Serializable]
public class GoldPerTapConfig
{
    public float multiplier = 5.0f;
    public float power = 2.1f;
}

[System.Serializable]
public class UpgradeCostConfig
{
    public float multiplier = 5.0f;
    public float powerBase = 1.08f;
}

[System.Serializable]
public class UnlockAttackerConfig
{
    public int baseCost = 100;
    public int multiplier = 10;
}
