﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

public class ConfigHandler : MonoBehaviour
{
    public UnityAction<GameConfig> onConfigDownload;
    
    private const string RemoteUrl = 
        "https://gist.githubusercontent.com/aykutyilmaz1" +
        "/1ee63deaddd015b78e42d718c6daaceb/raw/GameConfig.json";
    
    public void StartServerRequest()
    {
        StartCoroutine(DownloadConfig());
    }
    
    private IEnumerator DownloadConfig() 
    {
        UnityWebRequest www = new UnityWebRequest(RemoteUrl);
        
        www.downloadHandler = new DownloadHandlerBuffer();
        yield return www.SendWebRequest();
 
        if(www.isNetworkError || www.isHttpError) 
        {
            Debug.Log(www.error);
            
            //initialize from local config
            var config = GetDefaultGameConfig();
            onConfigDownload?.Invoke(config);
        }
        else
        {
            // Show results as text
            Debug.Log(www.downloadHandler.text);
            var config = GameConfig.
                CreateFromJSON(www.downloadHandler.text);

            if (config == null)
            {
                Debug.Log("Error in config  file!");
                config = GetDefaultGameConfig();
            }
            
            onConfigDownload?.Invoke(config);
        }
    }
    
    private GameConfig GetDefaultGameConfig()
    {
        Debug.Log("Initializing from default configs.");
        
        string filePath = "GameConfig";

        try
        {
            var dataAsJson = Resources.Load<TextAsset>(filePath);
            return GameConfig.CreateFromJSON(dataAsJson.ToString());
        }
        catch (System.Exception ex)
        {
            Debug.Log("No config found for game!");
        }

        return null;
    }
}
